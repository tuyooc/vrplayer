#import <Cocoa/Cocoa.h>
#import <Carbon/Carbon.h>
#import "mainapp.h"

#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

@interface GLView : NSOpenGLView
{
    omiAppBase *app;
    ivec2 dragStart;
    ivec2 mousePosition;
}
@end

@implementation GLView
+(NSOpenGLPixelFormat *)defaultPixelFormat
{
    NSOpenGLPixelFormatAttribute attr[] = {
        NSOpenGLPFAWindow,         // choose among pixelformats capable of rendering to windows
        NSOpenGLPFAAccelerated,    // require hardware-accelerated pixelformat
        NSOpenGLPFADoubleBuffer,   // require double-buffered pixelformat
        NSOpenGLPFAColorSize, 24,  // require 24 bits for color-channels
        NSOpenGLPFAAlphaSize, 8,   // require an 8-bit alpha channel
        NSOpenGLPFADepthSize, 24,  // require a 24-bit depth buffer
        NSOpenGLPFAMinimumPolicy,  // select a pixelformat which meets or exceeds these requirements
        0
    };
    return [[NSOpenGLPixelFormat alloc] initWithAttributes:attr];
}
-(void)prepareOpenGL
{
    app = new omiMainApp();
    app->m_basePath = [[[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/"] UTF8String];
    app->m_resourcePath = [[[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/"] UTF8String];
    app->m_screen = ivec2(_frame.size.width, _frame.size.height);
    if(!app->init())
       exit(0);
}
-(void)drawRect:(NSRect)dirtyRect
{
    [self.openGLContext makeCurrentContext];
    app->draw();
    [self.openGLContext flushBuffer];
}
-(void)reshape
{
    app->resize(_frame.size.width, _frame.size.height);
}
-(BOOL)acceptsFirstResponder
{
    return YES;
};
-(BOOL)becomeFirstResponder
{
    return YES;
}
-(void)keyDown:(NSEvent *)theEvent
{
    app->keyDown(theEvent.keyCode);
}
-(void)keyUp:(NSEvent *)theEvent
{
    app->keyUp(theEvent.keyCode);
}
-(void)mouseMoved:(NSEvent *)theEvent
{
    ivec2 pos = ivec2((int)theEvent.locationInWindow.x, (int)theEvent.locationInWindow.y);
    app->mouseMove(pos);
    mousePosition = pos;
}
-(void)otherMouseDown:(NSEvent *)theEvent { [self mouseDown:theEvent]; }
-(void)rightMouseDown:(NSEvent *)theEvent { [self mouseDown:theEvent]; }
-(void)mouseDown:(NSEvent *)theEvent
{
    dragStart = ivec2((int)theEvent.locationInWindow.x, (int)theEvent.locationInWindow.y);
    app->mouseDown(dragStart, (int)theEvent.buttonNumber);
}
-(void)otherMouseUp:(NSEvent *)theEvent { [self mouseUp:theEvent]; }
-(void)rightMouseUp:(NSEvent *)theEvent { [self mouseUp:theEvent]; }
-(void)mouseUp:(NSEvent *)theEvent
{
    app->mouseUp(ivec2((int)theEvent.locationInWindow.x, (int)theEvent.locationInWindow.y),
                 (int)theEvent.buttonNumber);
}
-(void)otherMouseDragged:(NSEvent *)theEvent { [self mouseDragged:theEvent]; }
-(void)rightMouseDragged:(NSEvent *)theEvent { [self mouseDragged:theEvent]; }
-(void)mouseDragged:(NSEvent *)theEvent
{
    ivec2 pos = ivec2((int)theEvent.locationInWindow.x, (int)theEvent.locationInWindow.y);
    app->mouseDrag(pos, dragStart, pos-mousePosition, (int)theEvent.buttonNumber);
    mousePosition = pos;
}
-(void)scrollWheel:(NSEvent *)theEvent
{
    ivec2 pos = ivec2((int)theEvent.locationInWindow.x, (int)theEvent.locationInWindow.y);
    vec2 delta = vec2(theEvent.deltaX, theEvent.deltaY);
    app->mouseWheel(pos, delta);
}
@end

@interface GLApp : NSApplication<NSWindowDelegate, NSApplicationDelegate>
{
    NSWindow *wnd;
    NSView *root;
    NSTimer *timer;
}
@end

@implementation GLApp
-(void)run
{
    @autoreleasepool
    {
        NSScreen *scr = [NSScreen screens][0];
        for(NSScreen *screen in [NSScreen screens])
            if(screen.frame.size.width == 1280 && screen.frame.size.height == 800)
                scr = screen;

        NSRect r = NSMakeRect(0, 0, 1280, 800);//*/NSMakeRect(0, 0, scr.frame.size.width, scr.frame.size.height);
        
        root = [[GLView alloc] initWithFrame:r];
        
//        NSInteger style = NSBorderlessWindowMask;
        NSInteger style = NSTitledWindowMask|NSClosableWindowMask|NSResizableWindowMask;
        wnd = [[NSWindow alloc] initWithContentRect:r
                                          styleMask:style
                                            backing:NSBackingStoreBuffered
                                              defer:NO
                                             screen:scr];
        wnd.delegate = self;
        wnd.contentView = root;
        wnd.acceptsMouseMovedEvents = YES;
        wnd.collectionBehavior = NSWindowCollectionBehaviorFullScreenPrimary;
        wnd.title = @"panoramic stereo vrplayer";
        [wnd makeKeyAndOrderFront:nil];
        //[wnd toggleFullScreen:self];
        
        timer = [NSTimer timerWithTimeInterval:1.0/30.0
                                        target:self
                                      selector:@selector(tick)
                                      userInfo:nil
                                       repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
        
        self.delegate = self;
    }
    _running = YES;
    while(_running)
    {
        @autoreleasepool
        {
            NSEvent* event = [self nextEventMatchingMask:NSAnyEventMask
                                               untilDate:nil
                                                  inMode:NSDefaultRunLoopMode
                                                 dequeue:YES];
            if(event)
            {
                if(event.type == NSKeyUp && event.keyCode == kVK_Escape)
                {
                    _running = NO;
                }
                [self sendEvent:event];
            }
        }
    }
}
-(void)tick
{
    root.needsDisplay = YES;
}
-(NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    return NSTerminateNow;
}
-(void)windowWillClose:(NSNotification *)notification
{
    _running = NO;
}
@end

int main(int argc, char *argv[])
{
    NSApplication *app = [GLApp sharedApplication];
    [app run];
    return 0;
}
