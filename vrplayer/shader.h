#ifndef _OMI_SHADER_
#define _OMI_SHADER_

#ifdef WIN32
    #include <Windows.h>
    #include <GL\glew.h>
    #include <gl\GL.h>
#else
    #include <OpenGL/gl.h>
#endif

#include <stdio.h>
#include <map>
#include <string>
#include <glm/glm.hpp>

using namespace std;
using namespace glm;

#define TEX_DIFFUSE     GL_TEXTURE0
#define TEX_NORMAL      GL_TEXTURE1
#define TEX_SPECULAR    GL_TEXTURE2
#define TEX_SHADOW      GL_TEXTURE3

#define ATT_TANGENTS        10
#define ATT_BONE_ID         1
#define ATT_BONE_WEIGHT     2

class omiShader
{
    GLuint m_prog;
    GLuint m_vert;
    GLuint m_frag;
    bool m_loaded;
    map<string, GLuint> m_locations;
    
    GLuint _loadShader(const string &source, GLenum type);
    GLuint _link();
    void _destroy();
    bool _checkCompilation(GLuint shader);
    bool _checkLinking(GLuint prog);
    bool _checkValidation(GLuint prog);
    
public:
    omiShader();
    bool load(const string &vertex, const string &fragment);
    void unload();
    void use();
    bool isLoaded();
    GLuint getUniformLocation(const string &name);
    bool operator== (const omiShader &b) const  { return m_prog == b.m_prog; }
};

#endif