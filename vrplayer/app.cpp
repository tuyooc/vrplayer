#include "app.h"
#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <stack>

using namespace Assimp;

omiAppBase *omiAppBase::s;

omiAppBase::omiAppBase()
{
    s = this;
    memset(m_keys, false, sizeof(m_keys));
}

float omiAppBase::randf()
{
    static float div = 1.0f / (float)INT_MAX;
    return (float)rand() * div;
}

float omiAppBase::randfn()
{
    static float div = 2.0f / (float)INT_MAX;
    return ((float)rand() * div) - 1.0f;
}

GLuint omiAppBase::loadTexture(const string &name, bool mipmaps)
{
    GLuint tex = 0;
    string filename = m_resourcePath + name;
    if(!name.empty() && (tex = m_textures[name]) == 0)
    {
        printf("Loading texture: %s\n", name.c_str());
        tex = SOIL_load_OGL_texture(filename.c_str(),
                                    SOIL_LOAD_AUTO,
                                    SOIL_CREATE_NEW_ID,
                                    SOIL_FLAG_INVERT_Y
                                    |SOIL_FLAG_TEXTURE_REPEATS
                                    |(mipmaps?SOIL_FLAG_MIPMAPS:0)
                                    );
        m_textures[name] = tex;
    }
    return tex;
}

void omiAppBase::drawGroup(const omiGroup &el, mat4 *transform)
{
    mat4 modelview = transform ? el.model * *transform : el.model;
    vector<omiMesh>::const_iterator iter;
    for(iter = el.meshes.begin(); iter != el.meshes.end(); ++iter)
    {
        drawElement((omiMesh&)*iter, &modelview);
    }
}

void omiAppBase::drawElement(omiElement &el, mat4 *transform)
{
    mat4 modelview;
    mat4 lmodelview;
    
    if(transform)
        modelview = m_view * el.model * *transform;
    else
        modelview = m_view * el.model;
    
    uniformMatrix4f("modelview", modelview);
    
    if(*m_currentShader == m_shaders["texture-shaded"])
    {
        if(transform)
            lmodelview = m_lview * el.model * *transform;
        else
            lmodelview = m_lview * el.model;
        
        uniformMatrix4f("lmodelview", lmodelview);
        uniformMatrix4f("tex_matrix", el.texmat);
        uniformMatrix3f("normal_matrix", inverseTranspose(mat3(modelview)));
        uniform3f("light_pos", vec3(m_view * vec4(m_lightPosition,1)));
        uniform4f("diffuse_color", el.color);
        uniform1i("enable_shading", el.shaded);
        
        uniform2f("diffuse_scale", vec2(1,1));
        uniform2f("specular_scale", vec2(1,1));
        uniform2f("normal_scale", vec2(1,1));

        uniform1i("has_specular_map", el.material.specular_tex > 0);
        uniform1i("has_normal_map", el.material.normal_tex > 0);
        uniform1i("has_diffuse_map", el.material.diffuse_tex > 0);
        uniform1i("has_shadow_map", el.material.shadow_tex > 0);
        
    }
    if(*m_currentShader == m_shaders["texture-shaded"] ||
       *m_currentShader == m_shaders["depth"])
    {
        uniform1i("has_tangent", el.has_tangent);
        uniform1i("has_bones", el.has_bones);
        if(el.has_bones)
        {
            float *mats = new float[16 * el.bones_vector.size()];
            for(int i = 0; i < el.bones_vector.size(); i++)
            {
                mat4 m = el.bones_vector[i]->trans() * el.bones_vector[i]->offset;
                memcpy(&mats[16*i], &m[0][0], sizeof(float[16]));
            }
            uniformMatrix4fv("bones", &mats[0], (int)el.bones.size());
            delete[] mats;
        }
    }
    el.draw(GL_TRIANGLES);
}

bool omiAppBase::createColorBuffer(const ivec2 &size, GLuint *outTexture, GLuint *outFBO)
{
    glGenTextures(1, outTexture);
    glBindTexture(GL_TEXTURE_2D, *outTexture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    // create a renderbuffer object to store depth info
    GLuint rboId;
    glGenRenderbuffers(1, &rboId);
    glBindRenderbuffer(GL_RENDERBUFFER, rboId);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size.x, size.y);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    
    // create a framebuffer object
    glGenFramebuffers(1, outFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, *outFBO);
    
    // attach the texture to FBO color attachment point
    glFramebufferTexture2D(GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER
                           GL_COLOR_ATTACHMENT0,  // 2. attachment point
                           GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
                           *outFBO,               // 4. tex ID
                           0);                    // 5. mipmap level: 0(base)
    
    // attach the renderbuffer to depth attachment point
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,      // 1. fbo target: GL_FRAMEBUFFER
                              GL_DEPTH_ATTACHMENT, // 2. attachment point
                              GL_RENDERBUFFER,     // 3. rbo target: GL_RENDERBUFFER
                              rboId);              // 4. rbo ID
    
    // check FBO status
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE)
        printf("createColorBuffer failed\n");
    
    // switch back to window-system-provided framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return status == GL_FRAMEBUFFER_COMPLETE;
}

bool omiAppBase::createDepthBuffer(const ivec2 &size, GLuint *outTexture, GLuint *outFBO)
{
    GLuint depthTexture;
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
    
    glTexImage2D(GL_TEXTURE_2D, 0,
                 GL_DEPTH_COMPONENT,
                 size.x, size.y, 0,
                 GL_DEPTH_COMPONENT,
                 GL_FLOAT, 0);
    
    // create a framebuffer object
    glGenFramebuffers(1, outFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, *outFBO);
    
    // Instruct openGL that we won't bind a color texture with the currently binded FBO
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    
    // attach the renderbuffer to depth attachment point
    glFramebufferTexture2D(GL_FRAMEBUFFER,
                           GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D,
                           depthTexture, 0);
    
    // check FBO status
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE)
        printf("createDepthBuffer failed\n");
    
    // switch back to window-system-provided framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    *outTexture = depthTexture;
    
    return status == GL_FRAMEBUFFER_COMPLETE;
}

static void print4(const char *s, mat4 m)
{
    printf("%s\n", s);
    for(int i = 0; i < 4; i++)
        printf("%.3f %.3f %.3f %.3f\n", m[0][i], m[1][i], m[2][i], m[3][i]);
}

bool omiAppBase::loadOBJ(const string &id, const string &name, const string &base)
{
    Importer importer;
    string basepath = m_resourcePath + base;
    string filename = basepath + name;
    
    const aiScene *scene = importer.ReadFile(filename,
                      aiProcess_CalcTangentSpace       |
                      aiProcess_Triangulate            |
                      aiProcess_JoinIdenticalVertices  |
                      aiProcess_SortByPType);
    
    
    // Load materials
    omiGroup &gr = m_groups[id];
    vector<omiMaterial> materials;
    for(int i = 0; i < scene->mNumMaterials; i++)
    {
        aiMaterial &mat = *scene->mMaterials[i];
        omiMaterial m;
        for(int j = 0; j < 14; j++)
        {
            if(mat.GetTextureCount((aiTextureType)j))
            {
                aiString tex;
                mat.GetTexture((aiTextureType)j, 0, &tex);
                if(tex.length)
                    printf("%d - %s\n", j, tex.C_Str());
            }
        }
        if(mat.GetTextureCount(aiTextureType_DIFFUSE))
        {
            aiString tex;
            mat.GetTexture(aiTextureType_DIFFUSE, 0, &tex);
            if(tex.length)
                m.diffuse_tex = loadTexture(base + tex.C_Str());
        }
        if(mat.GetTextureCount(aiTextureType_SPECULAR))
        {
            aiString tex;
            mat.GetTexture(aiTextureType_SPECULAR, 0, &tex);
            if(tex.length)
                m.specular_tex = loadTexture(base + tex.C_Str());
        }
        if(mat.GetTextureCount(aiTextureType_NORMALS))
        {
            aiString tex;
            mat.GetTexture(aiTextureType_NORMALS, 0, &tex);
            if(tex.length)
                m.normal_tex = loadTexture(base + tex.C_Str());
        }
        if(mat.GetTextureCount(aiTextureType_HEIGHT))
        {
            aiString tex;
            mat.GetTexture(aiTextureType_HEIGHT, 0, &tex);
            if(tex.length)
                m.normal_tex = loadTexture(base + tex.C_Str());
        }
        if(mat.GetTextureCount(aiTextureType_SHININESS))
        {
            aiString tex;
            mat.GetTexture(aiTextureType_SHININESS, 0, &tex);
            if(tex.length)
                m.normal_tex = loadTexture(base + tex.C_Str());
        }
        materials.push_back(m);
    }
    
    
    // Print scene hierarchy
    stack<aiNode*> nodes;
    stack<int> indents;
    string indent;
    aiNode *node = scene->mRootNode;
    while(node)
    {
        for(int i = 0; i < node->mNumChildren; i++)
        {
            nodes.push(node->mChildren[i]);
            indents.push((int)indent.length()+1);
        }
        
        printf("%s%s\n", indent.c_str(), node->mName.C_Str());
        
        if(nodes.size() == 0) break;
        
        node = nodes.top();
        nodes.pop();
        
        indent = "";
        for(int i = 0; i < indents.top(); i++)
            indent += "-";
        indents.pop();
    }
    
    gr.meshes = vector<omiMesh>(scene->mNumMeshes);
    for(int i = 0; i < scene->mNumMeshes; i++)
    {
        aiMesh *mesh = scene->mMeshes[i];
        omiMesh &m = gr.meshes[i];
        
        m.material = materials[mesh->mMaterialIndex];
        m.has_tangent = true;
        m.has_bones = mesh->mNumBones > 0;
        
        int *bone_count;
        float *bone_indices;
        float *bone_weights;
        if(mesh->mNumBones)
        {
            bone_count = new int[mesh->mNumVertices];
            bone_indices = new float[mesh->mNumVertices * 4];
            bone_weights = new float[mesh->mNumVertices * 4];
            memset(bone_count, 0, sizeof(int) * mesh->mNumVertices);
            memset(bone_indices, 0, sizeof(float) * mesh->mNumVertices * 4);
            memset(bone_weights, 0, sizeof(float) * mesh->mNumVertices * 4);
            for(int j = 0; j < mesh->mNumBones; j++)
            {
                aiBone *bone = mesh->mBones[j];
                aiNode *boneNode = scene->mRootNode->FindNode(bone->mName);
                omiBone &b = m.bones[bone->mName.C_Str()];
                m.bones_vector.push_back(&b);

                b.name = bone->mName.C_Str();
                b.mat = transpose(make_mat4(&boneNode->mTransformation[0][0]));
                b.offset = transpose(make_mat4(&bone->mOffsetMatrix[0][0]));
//                printf("%s\n", b.name.c_str());
//                print4("node mat", b.mat);
//                print4("offs mat", b.offset);
                //print4("bone mat", transpose(make_mat4(&bone->mOffsetMatrix[0][0])));
                if(m.bones.count(boneNode->mParent->mName.C_Str()))
                {
                    b.parent = &m.bones[boneNode->mParent->mName.C_Str()];
//                    b.computedMat = b.parent->computedMat * b.mat;
                }
                else
                {
//                    b.computedMat = b.mat;
                }
//                print4("computed", b.computedMat);
//                print4("computed inverse", inverse(b.computedMat));
//                print4("comp*off", b.computedMat*b.offset);
//                vec4 vert = b.computedMat * vec4(0,0,0,1);
//                printf("vertex: %.3f %.3f %.3f\n", vert.x, vert.y, vert.z);
//                printf("----------------\n");

                for(int k = 0; k < bone->mNumWeights; k++)
                {
                    aiVertexWeight &vw = bone->mWeights[k];
                    int idx = bone_count[vw.mVertexId];
                    aiVector3D aiv = mesh->mVertices[vw.mVertexId];
                    if(vw.mWeight > 0.0f && idx < 4)
                    {
                        bone_weights[vw.mVertexId * 4 + idx] = vw.mWeight;
                        bone_indices[vw.mVertexId * 4 + idx] = (float)j;
                        bone_count[vw.mVertexId]++;
                    }
                    else
                    {
                        printf("bones overflow bone:%d - vertex:%d(%.3f)\n", j, vw.mVertexId, vw.mWeight);
                    }
//                    printf("bone:%d vid:%d w:%.3f count:%d\n", j, vw.mVertexId, vw.mWeight, bone_count[vw.mVertexId]);
                }
                
            }
            
//            for(int j = 0; j < mesh->mNumVertices; j++)
//            {
//                aiVector3D &p = mesh->mVertices[j];
//                printf("vid: %d bones (tot %d/4): ", j, bone_count[j]);
//                for(int k = 0; k < bone_count[j] && k < 4; k++)
//                {
//                    int idx = j*4+k;
//                    printf("%d ", (int)bone_indices[idx]);
//                }
//                printf("\t");
//                float tot = 0;
//                for(int k = 0; k < 4; k++)
//                {
//                    int idx = j*4+k;
//                    printf("%.2f ", bone_weights[idx]);
//                    tot += bone_weights[idx];
//                }
//                printf(" = %.3f\t vec(%.3f, %.3f, %.3f)", tot, p.x, p.y, p.z);
//                printf("\n");
//            }
        }
        
        for(int j = 0; j < scene->mNumAnimations; j++)
        {
            aiAnimation *anim = scene->mAnimations[j];
            for(int k = 0; k < anim->mNumChannels; k++)
            {
                aiNodeAnim *nodeAnim = anim->mChannels[k];
                m.animFramesCount = std::max(m.animFramesCount, (int)nodeAnim->mNumRotationKeys);
                vector<omiKeyframe> &kframes = m.keyframes[nodeAnim->mNodeName.C_Str()];
                for(int h = 0; h < nodeAnim->mNumRotationKeys; h++)
                {
                    omiKeyframe kf;
                    aiQuaternion &q = nodeAnim->mRotationKeys[h].mValue;
                    kf.rotation.x = q.x;
                    kf.rotation.y = q.y;
                    kf.rotation.z = q.z;
                    kf.rotation.w = q.w;
                    kf.rotation = quat(q.w, q.x, q.y, q.z);
                    kf.time = (float)nodeAnim->mRotationKeys[h].mTime;
                    mat4 m = mat4_cast(kf.rotation);
                    printf("%s key %2d %.3f %.3f %.3f %.3f\n", nodeAnim->mNodeName.C_Str(), h, q.x, q.y, q.z, q.w);
                    print4("rotation", m);
                    kframes.push_back(kf);
                }
//                printf("key on %s\n", nodeAnim->mNodeName.C_Str());
            }
        }
        
        unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];
        for(int j = 0; j < mesh->mNumFaces; j++)
        {
            aiFace &f = mesh->mFaces[j];
            indices[j*3] = f.mIndices[0];
            indices[j*3+1] = f.mIndices[1];
            indices[j*3+2] = f.mIndices[2];
        }
        float *vertices = new float[mesh->mNumVertices * 3];
        float *normals = new float[mesh->mNumVertices * 3];
        float *tangents = new float[mesh->mNumVertices * 3];
        float *textures = new float[mesh->mNumVertices * 2];
        for(int j = 0; j < mesh->mNumVertices; j++)
        {
            aiVector3D &p = mesh->mVertices[j];
            aiVector3D &n = mesh->mNormals[j];
            aiVector3D &tg = mesh->mTangents[j];
            aiVector3D &t = mesh->mTextureCoords[0][j];
            vertices[j*3] = p.x;
            vertices[j*3+1] = p.y;
            vertices[j*3+2] = p.z;
            normals[j*3] = n.x;
            normals[j*3+1] = n.y;
            normals[j*3+2] = n.z;
            tangents[j*3] = tg.x;
            tangents[j*3+1] = tg.y;
            tangents[j*3+2] = tg.z;
            textures[j*2] = t.x;
            textures[j*2+1] = t.y;
        }
        
        long i_size = mesh->mNumFaces * 3 * sizeof(unsigned int);
        long p_size = mesh->mNumVertices * 3 * sizeof(float);
        long n_size = mesh->mNumVertices * 3 * sizeof(float);
        long tg_size = mesh->mNumVertices * 3 * sizeof(float);
        long t_size = mesh->mNumVertices * 2 * sizeof(float);
        long bid_size = mesh->mNumVertices * 4 * sizeof(float);
        long bw_size = mesh->mNumVertices * 4 * sizeof(float);
        
        glGenBuffers(1, &m.ibuf);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m.ibuf);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, indices, GL_STATIC_DRAW);
        
        m.i_count = mesh->mNumFaces * 3;
        m.p_offset = 0;
        m.n_offset = m.p_offset + p_size;
        m.tg_offset = m.n_offset + n_size;
        m.t_offset = m.tg_offset + tg_size;
        m.bid_offset = m.t_offset + t_size;
        m.bw_offset = m.bid_offset + bid_size;
        long tot_size = m.bones.size() > 0 ? m.bw_offset + bw_size : m.t_offset + t_size;
        glGenBuffers(1, &m.vbuf);
        glBindBuffer(GL_ARRAY_BUFFER, m.vbuf);
        glBufferData(GL_ARRAY_BUFFER, tot_size, NULL, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, m.p_offset, p_size, vertices);
        glBufferSubData(GL_ARRAY_BUFFER, m.n_offset, n_size, normals);
        glBufferSubData(GL_ARRAY_BUFFER, m.tg_offset, tg_size, tangents);
        glBufferSubData(GL_ARRAY_BUFFER, m.t_offset, t_size, textures);
        if(m.bones.size() > 0)
        {
            glBufferSubData(GL_ARRAY_BUFFER, m.bid_offset, bid_size, bone_indices);
            glBufferSubData(GL_ARRAY_BUFFER, m.bw_offset, bw_size, bone_weights);
        }
        
        delete[] indices;
        delete[] vertices;
        delete[] textures;
        delete[] normals;
        
        if(m.bones.size())
        {
            delete[] bone_count;
            delete[] bone_indices;
            delete[] bone_weights;
        }
    }
    
    importer.FreeScene();
    
    return true;
}

bool omiAppBase::loadShader(const string &name)
{
    omiShader &shader = m_shaders[name];
    char *vs = readFile(m_resourcePath + "/shaders/" + name + ".vs");
    char *fs = readFile(m_resourcePath + "/shaders/" + name + ".fs");
    if(shader.isLoaded())
        shader.unload();
    bool ret = shader.load(vs, fs);
    if(!ret) printf("Error loading shader %s\n", name.c_str());
    delete vs;
    delete fs;
    return ret;
}

void omiAppBase::useShader(const string &name)
{
    m_currentShader = &m_shaders[name];
    m_currentShader->use();
}

void omiAppBase::uniformMatrix4f(const string &name, const mat4 &mat)
{
    glUniformMatrix4fv(m_currentShader->getUniformLocation(name), 1, GL_FALSE, &mat[0][0]);
}

void omiAppBase::uniformMatrix4fv(const string &name, const float *mats, int count)
{
    glUniformMatrix4fv(m_currentShader->getUniformLocation(name), count, GL_FALSE, mats);
}

void omiAppBase::uniformMatrix3f(const string &name, const mat3 &mat)
{
    glUniformMatrix3fv(m_currentShader->getUniformLocation(name), 1, GL_FALSE, &mat[0][0]);
}

void omiAppBase::uniform1i(const string &name, GLuint n)
{
    glUniform1i(m_currentShader->getUniformLocation(name), n);
}

void omiAppBase::uniform2i(const string &name, const ivec2 &v)
{
    glUniform2i(m_currentShader->getUniformLocation(name), v.x, v.y);
}

void omiAppBase::uniform3i(const string &name, const ivec3 &v)
{
    glUniform3i(m_currentShader->getUniformLocation(name), v.x, v.y, v.z);
}

void omiAppBase::uniform4i(const string &name, const ivec4 &v)
{
    glUniform4i(m_currentShader->getUniformLocation(name), v.x, v.y, v.z, v.w);
}

void omiAppBase::uniform1f(const string &name, GLfloat f)
{
    glUniform1f(m_currentShader->getUniformLocation(name), f);
}

void omiAppBase::uniform2f(const string &name, const vec2 &v)
{
    glUniform2f(m_currentShader->getUniformLocation(name), v.x, v.y);
}

void omiAppBase::uniform3f(const string &name, const vec3 &v)
{
    glUniform3f(m_currentShader->getUniformLocation(name), v.x, v.y, v.z);
}

void omiAppBase::uniform4f(const string &name, const vec4 &v)
{
    glUniform4f(m_currentShader->getUniformLocation(name), v.x, v.y, v.z, v.w);
}

char* omiAppBase::readFile(const string &filename, long *length)
{
    ifstream f(filename, ios_base::in | ios_base::ate);
    long len = (long)f.tellg();
    f.seekg(0, ios_base::beg);
    f.clear();
    char* buffer = new char[len+1];
    memset(buffer, 0, len+1);
    f.read(buffer, len);
    f.close();
    if(length) *length = len;
    return buffer;
}

void omiAppBase::riftInit()
{
    System::Init(Log::ConfigureDefaultLog(LogMask_All));
    pManager = *DeviceManager::Create();
    pHMD = *pManager->EnumerateDevices<HMDDevice>().CreateDevice();
    if (pHMD)
    {
        pSensor = *pHMD->GetSensor();
        if(pHMD->GetDeviceInfo(&TheHMDInfo))
        {
            //RenderParams.MonitorName = hmd.DisplayDeviceName;
            stereo.SetHMDInfo(TheHMDInfo);
        }
        // Retrieve relevant profile settings.
        pUserProfile = pHMD->GetProfile();
    }
    else
    {
        pSensor = *pManager->EnumerateDevices<SensorDevice>().CreateDevice();
    }
    if(pSensor)
    {
        SFusion.AttachToSensor(pSensor);
        //SFusion.SetPredictionEnabled(true);
    }
    
    stereo.SetFullViewport(Viewport(0, 0, m_screen.x, m_screen.y));
    stereo.SetStereoMode(STEREO ? Stereo_LeftRight_Multipass : Stereo_None);
    stereo.SetDistortionFitPointVP(-1.0f, 0.0f);
    stereo.Set2DAreaFov(DegreeToRad(35.0f));
//    SFusion.Reset();
}

mat4 omiAppBase::riftOrient()
{
    Quatf    hmdOrient = SFusion.GetPredictedOrientation();
    Matrix4f hmdMat(hmdOrient);
    Matrix4f view = hmdMat;
    
    mat4 ret = make_mat4(&view.M[0][0]);
    return ret;
}

long omiAppBase::getTime()
{
#ifdef WIN32
    SYSTEMTIME st;
    GetSystemTime(&st);
    return ((st.wHour * 60 + st.wMinute) * 60 + st.wSecond) * 1000 + st.wMilliseconds;
#else
    struct timeval time;
    gettimeofday(&time, NULL);
    return (time.tv_sec * 1000) + (time.tv_usec / 1000);
#endif
}
