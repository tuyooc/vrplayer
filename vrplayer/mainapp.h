#ifndef _OMI_MAINAPP_
#define _OMI_MAINAPP_

#include "app.h"
#include "shader.h"
#include "primitive.h"
#include "tiny_obj_loader.h"
#include <assimp/Importer.hpp>
#include <theoraplayer/TheoraPlayer.h>
#include <theoraplayer/TheoraDataSource.h>

using namespace tinyobj;

class omiMainApp : public omiAppBase
{
    omiPlane m_plane;
    omiElement rSphere;
    
    TheoraVideoManager *m_theoraManager;
    TheoraVideoClip *m_theoraClipR;
    TheoraVideoClip *m_theoraClipL;
    
    vec2 m_rot;
    ivec2 m_rttSize;
    GLuint m_rttTexture;
    GLuint m_rttFBO;
    GLuint m_imageL;
    GLuint m_imageR;
    
    long timeStart;
    int frame;
    bool m_stereo;

public:
    omiMainApp();
    virtual ~omiMainApp(){};
    virtual bool init();
    virtual void draw();
    virtual void resize(int w, int h);
    virtual void mouseDown(const ivec2 &pos, int button);
    virtual void mouseUp(const ivec2 &pos, int button);
    virtual void mouseMove(const ivec2 &pos);
    virtual void mouseDrag(const ivec2 &pos, const ivec2 &start,
                           const ivec2 &delta, int button);
    virtual void mouseWheel(const ivec2 &pos, vec2 value);
    
    void renderEye(StereoEye eye);
    void drawVideo(GLuint tex_id, TheoraVideoClip* clip);
    GLuint createTexture(int w,int h,unsigned int format);
    int nextPow2(int x);
};

#endif
