#version 120
uniform mat4 proj;
uniform mat4 view;
varying vec4 color;
void main()
{
    gl_FragColor = color;
}
