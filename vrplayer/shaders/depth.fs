#version 120
varying vec4 pos;

void main()
{
    //vec2 p = vec2(gl_FragCoord.x / 512, gl_FragCoord.y / 512);
    vec4 p = pos / pos.w;
    float d = p.z;
    gl_FragColor = vec4(d);
}
