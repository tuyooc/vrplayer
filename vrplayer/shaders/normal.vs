#version 120
uniform mat4 bias;
uniform mat4 proj;
uniform mat4 modelview;
uniform mat3 normal_matrix;
uniform vec3 light_pos;

varying vec3 l;
varying vec3 e;

vec3 tangent(vec3 n)
{
    vec3 v1 = cross(n, vec3(0,0,1));
    vec3 v2 = cross(n, vec3(0,1,0));
    return length(v1) > length(v2) ? v1 : v2;
}

void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    
    vec3 n = normalize(normal_matrix * gl_Normal);
    vec3 v = vec3(modelview * gl_Vertex);
    
    vec3 t = normalize(normal_matrix * tangent(gl_Normal));
    vec3 b = cross(n,t);
    mat3 mat = mat3(t.x,b.x,n.x, t.y,b.y,n.y, t.z,b.z,n.z);
    
    l = mat * normalize(light_pos - v);
    e = mat * normalize(-v);
}

