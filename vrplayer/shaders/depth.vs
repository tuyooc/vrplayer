#version 120
uniform mat4 proj;
uniform mat4 modelview;

uniform bool has_bones;
uniform mat4 bones[32];
attribute vec4 bone_id;
attribute vec4 bone_weight;

varying vec4 pos;

void main()
{
    mat4 bonemat = mat4(1);
    ivec4 ids = ivec4(bone_id);
    vec4 vertex = gl_Vertex;
    if(has_bones)
    {
        bonemat = bones[ids[0]] * bone_weight[0] +
        bones[ids[1]] * bone_weight[1] +
        bones[ids[2]] * bone_weight[2] +
        bones[ids[3]] * bone_weight[3];
        vertex = bonemat * gl_Vertex;
    }

    gl_Position = proj * modelview * vertex;
    pos = gl_Position;
}
